# see https://github.com/polybar/polybar/issues/763 
# see https://github.com/polybar/polybar/issues/2078

# Kill polybar is present and Wait until the processes have been shut down
killall -q polybar
while pgrep -u $UID -x polybar > /dev/null; do sleep 1; done

# Set paths HWMON_PATH
for i in /sys/class/hwmon/hwmon*/temp*_input; do 
    if [ "$(<$(dirname $i)/name): $(cat ${i%_*}_label 2>/dev/null || echo $(basename ${i%_*}))" = "coretemp: Core 0" ]; then
        export HWMON_PATH="$i"
    fi
done

# Set bar on every monitor
for m in $(polybar --list-monitors | cut -d":" -f1); do
    MONITOR=$m polybar --reload k8x1d &
done
